

/* Classes to represent playing cards
 * and a deck of playing cards.
 *
 * Written by Albert "Anferensis" Ong.
 */

// ========================================================================

#include <iostream>
#include <algorithm>
#include <map>
#include <stdexcept>
#include <vector>

// ========================================================================

class Card
{
    public:
    
        std::string value;
        std::string suit;
        int rank;
        
        std::string name;
        
        
        /* The Card class constuctor.
         *
         * Takes three arguments: a value, a suit, and a rank.
         * Both the value and the suit,represented as strings, are required.
         * The ranking ,represented as an integer, is optional.
         */

        Card(std::string _value, std::string _suit, int _rank = 0)
        {
            
            
            std::string values[] = {"2", "3", "4", "5", "6", "7", "8", "9", "10", 
                                    "Jack", "Queen", "King", "Ace"};
            bool is_value = std::find(values, values + 13, _value) != values + 13;
            
            if (is_value) 
            {
                value = _value;
            }
            
            else
            {
                throw std::invalid_argument("The given value was not valid");
            }
            
            
            std::string suits[] = {"Spades", "Hearts", "Clubs", "Diamonds"};
            bool is_suit = std::find(suits, suits + 4, _suit) != suits + 4;
            
            if (is_suit)
            {
                suit = _suit;
            }
            
            else
            {
                throw std::invalid_argument ("The given suit was not valid");
            }
            
            name = _value + " of " + _suit;
            rank = _rank;
        }
        
        /* Overriding the output operator such that card objects
         * print out properly, as in:
         *
         * Card("Ace", "Spades") ---> Ace of Spades
         */
        
        friend std::ostream& operator<<(std::ostream& os, const Card &card)
        {
            os << card.name;
            return os;
        }
};



class Deck
{
    public:

        std::vector<Card> cards;
    
        Deck(std::vector<Card> _cards)
        {
            
            if (_cards.empty())
            {
                std::string suits[] = {"Spades", "Hearts", "Clubs", "Diamonds"};
                int suits_len = sizeof(suits) / sizeof(*suits);
                
                
                std::string values[] = {"2", "3", "4", "5", "6", "7", "8", "9", "10", 
                                        "Jack", "Queen", "King", "Ace"};
                int values_len = sizeof(values) / sizeof(*values);
                

                for (int s_index = 0; s_index < suits_len; s_index++)
                {
                    for (int v_index = 0; v_index < values_len; v_index++)
                    {
                        std::string value = values[v_index];
                        std::string suit = suits[s_index];
                    
                        _cards.push_back(Card(value, suit));
                    }
                }
            }
            cards = _cards;
        }
        
        void print();
        void setRanking(std::map <std::string, int> ranking);
        
        
        /*
         * I tried to override the output operator to print out the deck. 
         * But I ran into problems printing over multiple lines.
         */
            
        //~ friend std::ostream& operator<<(std::ostream& os, const Deck &deck)
        //~ {
            //~ for ( int index = 0; index < cards.size(); index++)
            //~ {
                //~ os << deck.cards[index];
            //~ }
            //~ return os;
        //~ }
};



void Deck::setRanking(std::map <std::string, int> ranking)
{
    ;
}



/* Couldn't find a good way to overload the output operator.
 * I decided to just build a method that prints out the deck.
 */

void Deck::print()
{
    int deck_size = this -> cards.size();
    
    for ( int index = 0; index < deck_size; index++)
    {
        std::cout << this -> cards[index] << "\n";
    }
}
    


// ========================================================================

int main() 
{
    std::vector<Card> cards;
    Deck deck(cards);
    
    deck.print();

    return 0;
}


