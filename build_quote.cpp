
/* 
 * Written by Albert"Anferensis"Ong
 * 
 * Properly formats a quote given the quote text and speaker
 * Designed for fireemblemwiki.org
 */
 
 
#include <iostream>
#include <string>

std::string build_quote(std::string quote, 
						std::string quote_speaker) {
							  
	bool is_quote_blank = quote == "";
	bool is_speaker_blank = quote_speaker == "";
	
	std::string formatted_quote;
	
	if (is_quote_blank or is_speaker_blank) {
		formatted_quote = "";
	}
	
	else {
		formatted_quote = "{{Quote|" + quote + "|" + quote_speaker + "}}";
	}
	
	return formatted_quote;	
}


int main() {
	
	std::string quote = "I fight for my friends";
	std::string quote_speaker = "[[Ike]]";
	
	std::cout << build_quote(quote, quote_speaker);
	
	
	return 0;
}

