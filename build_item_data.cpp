
/* 
 * Written by Albert"Anferensis"Ong
 * 
 * Constructs item data for fireemblemwiki.org
 */
 
#include <iostream>
#include <string>
#include <vector>


std::string build_item_data(std::string platform, 
							std::vector<std::vector<std::string>> item_data) {
	
	std::string formatted_item_data = "";
	
	int max_items = item_data.size();
	
	for (int index = 0; index < max_items; index++) {
		
		std::string item_num = "";
		
		if (index + 1 == max_items) {
			item_num = "last";
		}
		else {
			item_num = std::to_string(index + 1);
		}
		
		std::string item_name = item_data[index][0]; 
		std::string obtain_method = item_data[index][1];
		
		
		std::string item_line = "|item" + item_num + "=" + item_name;
		std::string obtain_line = "|obtain" + item_num + "=" + obtain_method;
		
		formatted_item_data += item_line + "\n";
		formatted_item_data += obtain_line + "\n";
	}
	
	std::string sections[4] = 
		{"===Item Data===", 
		"{{ChapItems", 
		"|platform=" + platform, 
		formatted_item_data + "}}"};
		
	std::string item_data_section = "";
	
	for (int index = 0; index < 4; index++) { 
		item_data_section += sections[index] + "\n";
	}
	
	return item_data_section;
}



int main() {
	
	// Insert platform
	// 	such as gba, gcn, or wii
	
	std::string platform = "gba";
	
	
	// Insert item data
	// Individual item data in formatted:
	//  	{item name, obtain method}, 
	
	// {"", ""}, 
	
	std::vector<std::vector<std::string>> item_data =
		{{"Iron Sword", "Obtain method 1"},
		 {"Iron Lance", "Obtain method 2"}, 
		 {"Iron Axe", "Obtain method 3"}};	
	
	std::cout << build_item_data(platform, item_data);
	
	
	return 0;
}

